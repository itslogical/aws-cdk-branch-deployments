# Branch deployments using Gitlab and AWS CDK

This repo provides a basic example on how to setup branch deployments using
Gitlab and [AWS CDK](https://docs.aws.amazon.com/cdk/latest/guide/home.html).

For this example we're using `python` but this can easily be replaced by 
another CDK supported language (e.g. TypeScript, JavaScript, Java or C#).

For the branch deployments to work in the pipeline ensure to set the 
environment variables to be available.

More specifically setup:
`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_REGION` under
`Settings > CI/CD > Variables`

# Setup 

Create a new CDK project
```
mkdir deploy && cd deploy
cdk init app --language python
```

Use the virtualenv 
```
python3 -m venv .venv
```

Install the dependencies 
```
pip install -r requirements.txt
```

# Development

The command `cdk deploy` relies on docker being available. In the Gitlab 
pipelines we can satisfy this condition by adding a service. 

For local development we can use DIND (docker-in-docker). A `docker-compose.yml`
has been added to demonstrate a working example.

```
docker-compose --env-file=.env up
```
