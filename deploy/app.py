#!/usr/bin/env python3
from aws_cdk import core as cdk

from aws_cdk import (
    aws_lambda_python as _lambda,
    aws_apigatewayv2 as apigwv2,
    aws_apigatewayv2_integrations as apigwv2ig,
    aws_appsync as appsync,
)

class DeployStack(cdk.Stack):
    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Create Lambda function
        hello_world_function = _lambda.PythonFunction(
            self, 'HelloWorldFunction',
            entry="../lambdas/hello_world",
            index="index.py",
            handler="handler"
        )
        hello_world_function_integration = apigwv2ig.LambdaProxyIntegration(handler=hello_world_function)

        # Setup HTTP API
        http_api = apigwv2.HttpApi(self, 'HttpApi')
        http_api.add_routes(path='/helloworld', methods=[apigwv2.HttpMethod.GET], integration=hello_world_function_integration)


        # Setup GraphQL API
        graphql_api = appsync.GraphqlApi(self,
                                 id='graphql_api',
                                 name='graphql_api',
                                 schema=appsync.Schema.from_asset("../schema.graphql")
                                 )
        lambda_ds = graphql_api.add_lambda_data_source(id='hello_world_data_source',
                                                       lambda_function=hello_world_function,
                                                       name='hello_world_data_source')
        lambda_ds.create_resolver(field_name='HelloWorld',
                                  type_name='Query')



app = cdk.App()
stack_name = app.node.try_get_context("stackName") or "DeployStack"

DeployStack(app, stack_name)

app.synth()
