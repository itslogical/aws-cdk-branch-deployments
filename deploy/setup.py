import setuptools

with open("../README.md", encoding="utf-8") as fp:
    long_description = fp.read()

setuptools.setup(
    name="deploy",
    version="0.0.1",

    description="Sample CDK Python app",
    long_description=long_description,
    long_description_content_type="text/markdown",

    author="author",

    package_dir={"": "."},
    packages=setuptools.find_packages(where="."),

    install_requires=[
        "aws-cdk.core==1.132.0",
        "aws-cdk.aws-lambda-python==1.132.0",
        "aws-cdk.aws-apigatewayv2==1.132.0",
        "aws-cdk.aws-apigatewayv2-integrations==1.132.0",
        "aws-cdk.aws-appsync==1.132.0",
    ],

    python_requires=">=3.6",

    classifiers=[
        "Development Status :: 4 - Beta",

        "Intended Audience :: Developers",

        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",

        "Topic :: Software Development :: Code Generators",
        "Topic :: Utilities",

        "Typing :: Typed",
    ],
)
